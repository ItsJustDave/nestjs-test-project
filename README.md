The application is written in Nestjs. THe ORM that is used is Prisma.The responses are structured in one way that is managed by /src/middlewares/interceptors/response-form.interceptor.ts.
The DB is consisted of two tables: User and UserFriends. The last one is related to User with two relations.
In order to handle file transfer "multer" package is used.Also custom decorator and interceptor is written on it's basis.The uploaded files are seated in public/protected_files.
There are two global error handling exceptions:Overall global filter and prisma erorr handler.
The entities are place in /src/routes.

---

Auth:
Current auth configuration uses jwt-strategy.In order to place the auth middleware on APIs a custom decorator and guard is written. ( auth/decorators | auth/guards)
The auth business-logic is managed by respective resource/module.

---

User:
The main business-logic is managed by corresponding resource/module.

---

In order to run the application needs to be called " npm run dev " after filling neccessary envs.
The required env form may be checked in default.env file.
