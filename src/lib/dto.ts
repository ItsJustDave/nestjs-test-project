import { Type } from "class-transformer";
import { IsNumber, IsOptional } from "class-validator";

export class PaginationQueryDto {
  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  page?: number = 1;

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  rows_per_page?: number = 10;
}
