import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { DbService } from "src/db/db.service";
import { LoginDto, UserRegisterDto, UserRegisterFilesDto } from "./dto";
import { LibService } from "src/lib/lib.service";
import { compare, genSalt, hash } from "bcryptjs";
import { NOT_AUTHENTICATED } from "src/lib/error-codes";
import { IChangePasswordToken, IResetToken } from "./types";
import { REQUEST_HAS_ENDED_SUCCESSFULLY } from "src/lib/common";

@Injectable()
export class AuthService {
  constructor(
    private dbService: DbService,
    private libService: LibService
  ) {}

  async usersRegister(body: UserRegisterDto & UserRegisterFilesDto) {
    const hashedPassword = await hash(body.password, await genSalt(10));
    body.password = hashedPassword;
    const user = await this.dbService.user.create({
      data: {
        ...body
      }
    });
    return { token: this.libService.signToken({ id: user.id, type: "user" }) };
  }

  async usersLogin(body: LoginDto) {
    const user = await this.dbService.user.findFirst({
      select: { id: true, password: true },
      where: { OR: [{ email: body.login }, { phone: body.login }] }
    });

    const password_matches = await compare(body.password, user.password);
    if (!user || !password_matches) throw new HttpException(NOT_AUTHENTICATED, HttpStatus.UNAUTHORIZED);

    return { token: this.libService.signToken({ id: user.id, type: "user" }) };
  }

  async sendCodeForForgetPassword(email: string) {
    const user = await this.dbService.user.findFirstOrThrow({
      where: { email: email }
    });
    const random = (Math.floor(Math.random() * (999999 - 100000)) + 100000).toString();
    const hashed_random = await hash(random, await genSalt(10));

    await this.libService.sendMail("RESET CODE", random, user.email);

    const reset_token = this.libService.signToken({ hash: hashed_random, user_id: user.id }, "10m");

    return { reset_token };
  }

  async confirmCodeForForgetPassword(code: string, token_payload: IResetToken) {
    if (!(await compare(code, token_payload.hash)))
      throw new HttpException({ message: "Code does not matches", code: 400 }, HttpStatus.UNAUTHORIZED);

    const change_password_token = this.libService.signToken({ user_id: token_payload.user_id }, "3m");

    return { change_password_token };
  }

  async changePasswordForForgetPassword(password: string, token_payload: IChangePasswordToken) {
    const hashedPassword = await hash(password, await genSalt(10));

    await this.dbService.user.update({ data: { password: hashedPassword }, where: { id: token_payload.user_id } });

    return REQUEST_HAS_ENDED_SUCCESSFULLY;
  }
}
