import { DbService } from "src/db/db.service";
import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, "jwt") {
  constructor(
    config: ConfigService,
    private db: DbService
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.get<string>("JWT_SECRET")
    });
  }

  async validate(payload: { id: string }) {
    try {
      const user = await this.db.user.findUniqueOrThrow({
        where: { id: payload.id }
      });
      return user;
    } catch (err) {
      throw new HttpException({ message: "Unauthorized", code: 4011 }, HttpStatus.UNAUTHORIZED);
    }
  }
}
