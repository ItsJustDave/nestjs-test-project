import { Type } from "class-transformer";
import { IsEmail, IsNotEmpty, IsOptional, IsPhoneNumber, IsString, MaxLength, MinLength } from "class-validator";

export class UserRegisterDto {
  @MaxLength(63)
  @IsString()
  @IsNotEmpty()
  firstname: string;

  @MaxLength(63)
  @IsString()
  @IsNotEmpty()
  lastname: string;

  @Type(() => Number)
  @IsNotEmpty()
  age: number;

  @IsPhoneNumber()
  @IsOptional()
  phone: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @MinLength(8)
  @IsString()
  @IsNotEmpty()
  password: string;
}

export class UserRegisterFilesDto {
  @IsOptional()
  avatar: string;
}

export class LoginDto {
  @IsString()
  @IsNotEmpty()
  login: string;

  @MinLength(8)
  @IsString()
  @IsNotEmpty()
  password: string;
}

export class SendCodeDto {
  @MaxLength(127)
  @IsString()
  @IsNotEmpty()
  email: string;
}

export class ConfirmCodeDto {
  @MaxLength(6)
  @IsString()
  @IsNotEmpty()
  code: string;
}

export class ChangePasswordDto {
  @MinLength(8)
  @IsString()
  @IsNotEmpty()
  password: string;
}
