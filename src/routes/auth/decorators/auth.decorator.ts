import { JwtGuard } from "./../guards/jwt.guard";
import { applyDecorators, UseGuards } from "@nestjs/common";

export function Auth() {
  return applyDecorators(UseGuards(JwtGuard));
}
