import { Body, Controller, Headers, HttpCode, HttpException, HttpStatus, Post, Put, UseInterceptors } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { ChangePasswordDto, ConfirmCodeDto, LoginDto, SendCodeDto, UserRegisterDto, UserRegisterFilesDto } from "./dto";
import { FileInterceptor } from "@nestjs/platform-express";
import multerOptions from "src/middlewares/interceptors/multer";
import { Files } from "src/decorators/files.decorator";
import { IChangePasswordToken, IResetToken } from "./types";
import { LibService } from "src/lib/lib.service";
import { INTERNAL_SERVER_ERROR } from "src/lib/error-codes";

@Controller("auth")
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly libService: LibService
  ) {}

  @HttpCode(200)
  @Post("/users/register")
  @UseInterceptors(FileInterceptor("avatar", multerOptions()))
  usersRegister(@Body() body: UserRegisterDto, @Files({ optimize: true }) files: UserRegisterFilesDto) {
    return this.authService.usersRegister(Object.assign(body, files));
  }

  @HttpCode(200)
  @Post("/users/login")
  usersLogin(@Body() body: LoginDto) {
    return this.authService.usersLogin(body);
  }

  @HttpCode(200)
  @Post("/users/forget-password/send-code")
  sendCodeForForgetPassword(@Body() body: SendCodeDto) {
    return this.authService.sendCodeForForgetPassword(body.email);
  }

  @HttpCode(200)
  @Post("/users/forget-password/confirm-code")
  confirmCodeForForgetPassword(@Body() body: ConfirmCodeDto, @Headers("reset_token") reset_token: string) {
    try {
      const token = this.libService.verifyToken<IResetToken>(reset_token);

      return this.authService.confirmCodeForForgetPassword(body.code, token);
    } catch (err) {
      if (err?.key == "jwt_error") {
        throw new HttpException({ message: "Wrong reset_token", code: 4060 }, HttpStatus.NOT_ACCEPTABLE);
      }
      throw new HttpException(INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Put("/users/forget-password/change-password")
  changePasswordForForgetPassword(
    @Body() body: ChangePasswordDto,
    @Headers("change_password_token") change_password_token: string
  ) {
    try {
      const token = this.libService.verifyToken<IChangePasswordToken>(change_password_token);

      return this.authService.changePasswordForForgetPassword(body.password, token);
    } catch (err) {
      if (err?.key == "jwt_error") {
        throw new HttpException({ message: "wrong change_password_token", code: 4060 }, HttpStatus.NOT_ACCEPTABLE);
      }
      throw new HttpException(INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
