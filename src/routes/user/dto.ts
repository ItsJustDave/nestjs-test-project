import { FriendRequestStatuses } from "@prisma/client";
import { Type } from "class-transformer";
import { IsIn, IsNotEmpty, IsOptional, IsString } from "class-validator";
import { PaginationQueryDto } from "src/lib/dto";

export class SearchUsersQueryDto extends PaginationQueryDto {
  @IsString()
  @IsOptional()
  firstname: string;

  @IsString()
  @IsOptional()
  lastname: string;

  @Type(() => Number)
  @IsOptional()
  age: number;
}

export class AddUserToFriendsDto {
  @IsString()
  @IsNotEmpty()
  target_id: string;
}

export class ReactToFriendRequest {
  @IsIn([FriendRequestStatuses.accepted, FriendRequestStatuses.declined])
  @IsNotEmpty()
  reaction: FriendRequestStatuses;
}
