import { Body, Controller, Get, Param, ParseIntPipe, Patch, Post, Query } from "@nestjs/common";
import { UserService } from "./user.service";
import { Auth } from "../auth/decorators/auth.decorator";
import { AddUserToFriendsDto, ReactToFriendRequest, SearchUsersQueryDto } from "./dto";
import { GetUser } from "../auth/decorators/user.decorator";
import { FriendRequestStatuses, User } from "@prisma/client";
import { DbService } from "src/db/db.service";
import { REQUEST_HAS_ENDED_SUCCESSFULLY } from "src/lib/common";
@Auth()
@Controller("users")
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly dbService: DbService
  ) {}

  @Get("search")
  searchUsers(@Query() query: SearchUsersQueryDto, @GetUser() user: User) {
    return this.userService.searchUsers(query, user.id);
  }

  @Get("/:id")
  getCurrentUser(@Param("id") id: string) {
    return this.userService.getCurrentUser(id);
  }

  @Get("friend-requests")
  getFriendRequestList(@GetUser() user: User) {
    return this.userService.getFriendRequestList(user.id);
  }

  @Post("friend-request")
  addUserToFriendlist(@Body() body: AddUserToFriendsDto, @GetUser() user: User) {
    return this.userService.addUserToFriendlist(body.target_id, user.id);
  }

  @Patch("friend-requests/:id/react")
  async reactToFriendRequest(@GetUser() user: User, @Param("id", ParseIntPipe) id: number, @Body() body: ReactToFriendRequest) {
    await this.dbService.userFriend.update({
      where: { id, status: FriendRequestStatuses.requested, target_id: user.id },
      data: {
        status: body.reaction
      }
    });

    return REQUEST_HAS_ENDED_SUCCESSFULLY;
  }
}
