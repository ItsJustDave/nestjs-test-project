import { Injectable } from "@nestjs/common";
import { DbService } from "src/db/db.service";
import { SearchUsersQueryDto } from "./dto";
import { FriendRequestStatuses } from "@prisma/client";
import { REQUEST_HAS_ENDED_SUCCESSFULLY } from "src/lib/common";

@Injectable()
export class UserService {
  constructor(private readonly dbService: DbService) {}

  async searchUsers(query: SearchUsersQueryDto, id: string) {
    const { page, rows_per_page } = query;

    // users are filtered based on "or,or" logic and exludes the requester.The result is paginated.
    const filtered_users = await this.dbService.user.findMany({
      where: {
        id: { not: id },
        OR: [
          {
            firstname: { contains: query.firstname }
          },
          {
            lastname: { contains: query.lastname }
          },
          {
            age: query.age
          }
        ]
      },
      select: {
        id: true,
        firstname: true,
        lastname: true,
        age: true,
        avatar: true
      },
      skip: page * rows_per_page - rows_per_page,
      take: rows_per_page
    });

    return { items: filtered_users };
  }

  async getFriendRequestList(target_id: string) {
    const requesters_list = await this.dbService.userFriend.findMany({
      where: { target_id, status: FriendRequestStatuses.requested },
      select: {
        requester: {
          select: {
            id: true,
            firstname: true,
            lastname: true,
            avatar: true
          }
        }
      }
    });

    return { items: requesters_list.map(el => el.requester) };
  }

  async getCurrentUser(id: string) {
    const user = await this.dbService.user.findFirstOrThrow({
      where: { id },
      select: {
        id: true,
        firstname: true,
        lastname: true,
        avatar: true,
        age: true
      }
    });

    return { item: user };
  }

  async addUserToFriendlist(target_id: string, requester_id: string) {
    // creates the resource with status "requested".No need vor validation as [target_od,requester_id] is unique.
    await this.dbService.userFriend.create({
      data: {
        requester_id,
        target_id
      }
    });

    return REQUEST_HAS_ENDED_SUCCESSFULLY;
  }
}
