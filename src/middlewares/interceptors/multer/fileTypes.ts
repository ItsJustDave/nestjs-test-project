import { MulterField } from "@nestjs/platform-express/multer/interfaces/multer-options.interface";
// import { Field } from "multer";

export const possibleFiles: { [key: string]: MulterField[] } = {
  // company_register: [{ name: "avatar", maxCount: 1 }, { name: "files" }]
};
const types: Record<string, Record<string, { mime_type?: string; format?: string }>> = {
  about: { video: { mime_type: "video" }, image: { mime_type: "image" } }
};

export default types;
