import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { DbModule } from "./db/db.module";
import { ConfigModule } from "@nestjs/config";
import { ConfigModuleDto } from "./lib/common";
import { LibModule } from "./lib/lib.module";
import { AuthModule } from "./routes/auth/auth.module";
import { UserModule } from "./routes/user/user.module";
@Module({
  imports: [DbModule, ConfigModule.forRoot(ConfigModuleDto), LibModule, AuthModule, UserModule],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
