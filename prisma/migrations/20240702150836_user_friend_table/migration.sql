-- CreateTable
CREATE TABLE `user_friends` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `target_id` VARCHAR(63) NOT NULL,
    `requester_id` VARCHAR(63) NOT NULL,
    `status` ENUM('requested', 'accepted', 'declined') NOT NULL DEFAULT 'requested',

    UNIQUE INDEX `user_friends_target_id_requester_id_key`(`target_id`, `requester_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `user_friends` ADD CONSTRAINT `UserFriend-User_fk0` FOREIGN KEY (`requester_id`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `user_friends` ADD CONSTRAINT `UserFriend-User_fk1` FOREIGN KEY (`target_id`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
