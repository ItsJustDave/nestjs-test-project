-- CreateTable
CREATE TABLE `users` (
    `id` VARCHAR(63) NOT NULL,
    `fullname` VARCHAR(63) NOT NULL,
    `avatar` VARCHAR(127) NULL,
    `phone` VARCHAR(63) NULL,
    `email` VARCHAR(127) NOT NULL,
    `password` VARCHAR(127) NOT NULL,

    UNIQUE INDEX `phone`(`phone`),
    UNIQUE INDEX `email`(`email`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
